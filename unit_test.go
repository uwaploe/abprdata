package abprdata

import (
	"context"
	"math/rand"
	"path/filepath"
	reflect "reflect"
	"testing"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/afero"
)

func TestFilename(t *testing.T) {
	table := []struct {
		in  string
		out string
	}{
		{
			in:  "2020-01-01T01:23:45Z",
			out: "foo/15/2020-001-15.bin",
		},
		{
			in:  "2020-01-01T01:15:00Z",
			out: "foo/15/2020-001-15.bin",
		},
		{
			in:  "2019-12-31T17:15:00-08:00",
			out: "foo/15/2020-001-15.bin",
		},
	}

	store := NewDataStore("foo")
	for _, e := range table {
		ts, err := time.Parse(time.RFC3339, e.in)
		if err != nil {
			t.Fatal(err)
		}
		name := store.filenameFromTime(ts)
		if name != e.out {
			t.Errorf("Bad value; expected %q, got %q", e.out, name)
		}
	}
}

type prMeas struct {
	pr, temp float64
}

func (m prMeas) Pr() float64 {
	return rand.NormFloat64()*0.05 + 4197.0
}

func (m prMeas) Temp() float64 {
	return rand.NormFloat64()*0.005 - 0.5
}

func helperFillStore(t *testing.T, nrecs int) *DataStore {
	fs := afero.NewMemMapFs()
	store := NewDataStoreFs("data", fs)
	m := store.genMeta()
	if m.Nrecs != 0 {
		t.Fatalf("Unexpected record count: %d", m.Nrecs)
	}

	sample := prMeas{}
	for i := 0; i < nrecs; i++ {
		rec := NewRecord(time.Unix(int64(i*900), 0), sample)
		err := store.AddRecord(rec)
		if t != nil && err != nil {
			t.Fatal(err)
		}
	}
	store.Close()

	return store
}

func TestFileStore(t *testing.T) {
	nrecs := 100
	store := helperFillStore(t, nrecs)

	if store.meta.Nrecs != nrecs {
		t.Errorf("Wrong record count; expected %d, got %d",
			nrecs, store.meta.Nrecs)
	}

	meta := store.genMeta()
	if !reflect.DeepEqual(meta, store.meta) {
		t.Errorf("Metadata generation failed; expected %#v, got %#v",
			store.meta, meta)
	}

	drange := store.Range()
	expected := &DataRange{Start: 0, End: (int64(nrecs) - 1) * 900, Count: 100}
	if !proto.Equal(&drange, expected) {
		t.Errorf("Wrong data range; expected %#v, got %#v", expected, &drange)
	}

	paths := map[string][]string{
		"data/00": {"1970-001-00.bin", "1970-002-00.bin"},
		"data/15": {"1970-001-15.bin", "1970-002-15.bin"},
		"data/30": {"1970-001-30.bin", "1970-002-30.bin"},
		"data/45": {"1970-001-45.bin", "1970-002-45.bin"},
	}

	for dir, files := range paths {
		ok, err := afero.DirExists(store.fs, dir)
		if err != nil {
			t.Fatal(err)
		}

		if !ok {
			t.Errorf("Expected path %q not found", dir)
		}

		for _, file := range files {
			path := filepath.Join(dir, file)
			ok, err := afero.Exists(store.fs, path)
			if err != nil {
				t.Fatal(err)
			}

			if !ok {
				t.Errorf("Expected path %q not found", dir)
			}
		}
	}

	ok, _ := afero.Exists(store.fs, filepath.Join("data", "metadata"))
	if !ok {
		t.Error("Data store metadata not saved")
	}
}

func TestRecordRead(t *testing.T) {
	nrecs := 100
	store := helperFillStore(t, nrecs)

	view := store.NewView(15)
	dstart, _ := Date("1970-01-01")
	ch := view.Walk(context.Background(), dstart, 0)

	count := 0
	for _ = range ch {
		count++
	}
	if count != nrecs/4 {
		t.Errorf("Bad record count; expected %d, got %d",
			nrecs/4, count)
	}
}

func TestRecordLimit(t *testing.T) {
	nrecs := 200
	store := helperFillStore(t, nrecs)
	days := 1
	limit := days * 24

	view := store.NewView(15)
	dstart, _ := Date("1970-01-01")
	ch := view.Walk(context.Background(), dstart, days)

	count := 0
	for _ = range ch {
		count++
	}
	if count != limit {
		t.Errorf("Bad record count; expected %d, got %d",
			limit, count)
	}
}

func TestCompress(t *testing.T) {
	nrecs := 90 * 24
	store := helperFillStore(t, nrecs)
	view := store.NewView(15)
	dstart, _ := Date("1970-01-01")
	ch := view.Walk(context.Background(), dstart, 0)

	s := &Store{}
	s.Records = make([]*Record, 0)
	for rec := range ch {
		r := rec
		s.Records = append(s.Records, r)
	}

	ds := s.Compress()
	s2 := ds.Expand()
	for i, rec := range s.Records {
		if !proto.Equal(rec, s2.Records[i]) {
			t.Errorf("Mismatch on record %d", i)
			break
		}
	}

	t.Logf("Uncompressed size: %d bytes", proto.Size(s))
	t.Logf("Compressed size: %d bytes", proto.Size(ds))
}

func TestPartialRead(t *testing.T) {
	nrecs := 100
	store := helperFillStore(t, nrecs)

	view := store.NewView(15)
	dstart, _ := Date("1970-01-02")
	ch := view.Walk(context.Background(), dstart, 0)

	count := 0
	for _ = range ch {
		count++
	}
	if count != 1 {
		t.Errorf("Bad record count; expected 1, got %d",
			count)
	}
}

func TestSub(t *testing.T) {
	table := []struct {
		a, b *Record
		val  *Delta
	}{
		{
			a:   &Record{Pressure: 42},
			b:   &Record{Pressure: 43},
			val: &Delta{Pressure: -1},
		},
	}

	for _, e := range table {
		val := e.a.Sub(e.b)
		if !proto.Equal(val, e.val) {
			t.Errorf("Sub failed; expected %#v, got %#v", e.val, val)
		}
	}
}

func TestAdd(t *testing.T) {
	table := []struct {
		a   *Record
		b   *Delta
		val *Record
	}{
		{
			a:   &Record{Pressure: 43},
			b:   &Delta{Pressure: -1},
			val: &Record{Pressure: 42},
		},
	}

	for _, e := range table {
		val := e.a.Add(e.b)
		if !proto.Equal(val, e.val) {
			t.Errorf("Add failed; expected %#v, got %#v", e.val, val)
		}
	}
}

func TestDataReq(t *testing.T) {
	req := &DataReq{Year: 2020, Month: 1, Day: 1, Minute: 30}
	buf := proto.NewBuffer([]byte{0x42})
	err := buf.Marshal(req)
	if err != nil {
		t.Fatal(err)
	}

	b := buf.Bytes()
	req2 := &DataReq{}
	err = proto.Unmarshal(b[1:], req2)
	if err != nil {
		t.Fatal(err)
	}

	if req.String() != req2.String() {
		t.Errorf("Mismatch; expected %q, got %q", req, req2)
	}
}

func TestCsv(t *testing.T) {
	table := []struct {
		in  Record
		out string
	}{
		{
			in:  Record{Time: 0, Pressure: 456789, Temperature: 123456},
			out: "\"1970-01-01 00:00:00\",456.789,123.456",
		},
	}

	for _, e := range table {
		out := e.in.AsCsv()
		if out != e.out {
			t.Errorf("Value mismatch; expected %q, got %q", e.out, out)
		}
	}
}

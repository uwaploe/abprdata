package abprdata

// Sub returns the Delta r-x.
func (r *Record) Sub(x *Record) *Delta {
	dr := &Delta{}

	dr.Time = r.Time - x.Time
	dr.Pressure = int32(r.Pressure) - int32(x.Pressure)
	dr.Temperature = r.Temperature - x.Temperature

	return dr
}

// Add returns the Record r+dr
func (r *Record) Add(dr *Delta) *Record {
	x := &Record{}

	x.Time = r.Time + dr.Time
	x.Pressure = uint32(int32(r.Pressure) + dr.Pressure)
	x.Temperature = r.Temperature + dr.Temperature

	return x
}

// Compress returns a delta-compressed version of s. The actually storage compression
// occurs when the DeltaStore is marshaled to its wire format.
func (s *Store) Compress() *DeltaStore {
	ds := &DeltaStore{}
	ds.Ref = s.Records[0]
	n := len(s.Records)
	ds.Diffs = make([]*Delta, 0, n-1)
	for i := 1; i < n; i++ {
		ds.Diffs = append(ds.Diffs, s.Records[i].Sub(s.Records[i-1]))
	}

	return ds
}

// Expand returns an uncompressed version of ds.
func (ds *DeltaStore) Expand() *Store {
	s := &Store{}
	n := len(ds.Diffs)
	s.Records = make([]*Record, 0, n+1)
	s.Records = append(s.Records, ds.Ref)
	for i := 0; i < n; i++ {
		s.Records = append(s.Records, s.Records[i].Add(ds.Diffs[i]))
	}

	return s
}

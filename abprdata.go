// Package abprdata implements the ABPR data store. The data store is designed
// for low-frequency writes. During deployment, the system will wakeup every
// 15 minutes, acquire a single data record, write it to the store and
// shutdown.
package abprdata

import (
	"context"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"github.com/spf13/afero"
	"google.golang.org/protobuf/proto"
)

//go:generate protoc -I=. --go_out=. --go_opt=paths=source_relative abprdata.proto

const (
	SampleScale float64 = 1e3
	EngScale    float32 = 1e3
	TiltScale   float32 = 1e3
)

// PrSample represents a temperature compensated pressure measurement
type PrSample interface {
	// Pr returns a pressure value in meters of seawater
	Pr() float64
	// Temp returns a temperature value in degrees C
	Temp() float64
}

// EngSample represents an engineering data sample
type EngSample interface {
	// V returns a battery voltage value in volts
	V() float32
	// I returns a current draw value in amps
	I() float32
}

// TiltSample represents a tilt sensor measurement
type TiltSample interface {
	// Pitch returns the pitch angle in degrees
	Pitch() float32
	// Roll returns the roll angle in degrees
	Roll() float32
}

// NewRecord creates a new Record from a pressure sample
func NewRecord(t time.Time, sample PrSample) *Record {
	return &Record{
		Time:        t.Unix(),
		Pressure:    uint32(sample.Pr() * SampleScale),
		Temperature: int32(sample.Temp() * SampleScale),
	}
}

const CsvTimeFormat = "2006-01-02 15:04:05"

// AsCsv returns a string containing the Record fields in CSV format. The
// fields are converted to unscaled (floating point) values.
func (rec *Record) AsCsv() string {
	t := time.Unix(rec.Time, 0)
	return fmt.Sprintf("%q,%.3f,%.3f", t.UTC().Format(CsvTimeFormat),
		float64(rec.Pressure)/SampleScale,
		float64(rec.Temperature)/SampleScale)
}

type StatusSample interface {
	PrSample
	TiltSample
	// Vertical speed in m/s
	Speed() float64
}

// NewStatus creates a new Status message
func NewStatus(t time.Time, sample StatusSample) *Status {
	return &Status{
		Time:        t.Unix(),
		Pressure:    uint32(sample.Pr() * SampleScale),
		Temperature: int32(sample.Temp() * SampleScale),
		Pitch:       int32(sample.Pitch() * TiltScale),
		Roll:        int32(sample.Roll() * TiltScale),
		Speed:       int32(sample.Speed() * SampleScale),
	}
}

func (s *Status) AsCsv() string {
	t := time.Unix(s.Time, 0)
	return fmt.Sprintf("%q,%.3f,%.2f,%.1f.%.1f,%.2f", t.UTC().Format(CsvTimeFormat),
		float64(s.Pressure)/SampleScale,
		float64(s.Temperature)/SampleScale,
		float32(s.Pitch)/TiltScale,
		float32(s.Roll)/TiltScale,
		float64(s.Speed)/SampleScale)
}

// Dstart returns a time.Time corresponding to 00:00:00 UTC on the date
// of the DataReq.
func (dr *DataReq) Dstart() time.Time {
	return time.Date(int(dr.Year),
		time.Month(dr.Month),
		int(dr.Day),
		0, 0, 0, 0, time.UTC)
}

type metadata struct {
	Tstart time.Time `json:"t_start"`
	Tend   time.Time `json:"t_end"`
	Nrecs  int       `json:"nrecs"`
}

// DataStore manages the on disk storage of ABPR data records. The data
// records are grouped by the minute-of-the-hour interval in which they are
// collected.
type DataStore struct {
	fs       afero.Fs
	rootDir  string
	interval int
	meta     metadata
}

// NewDataStore returns a new DataStore stored in the directory root
func NewDataStore(root string) *DataStore {
	return NewDataStoreFs(root, nil)
}

// NewDataStoreFs returns a new DataStore implemented on an afero.Fs
// fileystem interface under the directory root.
func NewDataStoreFs(root string, fs afero.Fs) *DataStore {
	s := DataStore{
		rootDir:  root,
		interval: 15,
	}

	if fs != nil {
		s.fs = fs
	} else {
		s.fs = afero.NewOsFs()
	}

	mdpath := filepath.Join(s.rootDir, "metadata")
	if in, err := afero.ReadFile(s.fs, mdpath); err == nil {
		json.Unmarshal(in, &s.meta)
	} else {
		// Could not load metadata, generate it.
		s.meta = s.genMeta()
	}

	return &s
}

// Close writes the metadata to disk.
func (s *DataStore) Close() error {
	f, err := s.fs.Create(filepath.Join(s.rootDir, "metadata"))
	if err != nil {
		return err
	}
	defer f.Close()

	return json.NewEncoder(f).Encode(s.meta)
}

// Range returns the current time range of the DataStore
func (s *DataStore) Range() DataRange {
	if in, err := afero.ReadFile(s.fs, filepath.Join(s.rootDir, "metadata")); err == nil {
		json.Unmarshal(in, &s.meta)
	}
	return DataRange{
		Start: s.meta.Tstart.Unix(),
		End:   s.meta.Tend.Unix(),
		Count: uint32(s.meta.Nrecs),
	}
}

func (s *DataStore) genMeta() metadata {
	var m metadata

	for minute := 0; minute < 60; minute += s.interval {
		v := s.NewView(minute)
		for rec := range v.Walk(context.Background(), time.Time{}, 0) {
			m.Nrecs += 1
			t := time.Unix(rec.Time, 0).UTC()
			if t.After(m.Tend) {
				m.Tend = t
			}
			if m.Tstart.IsZero() || t.Before(m.Tstart) {
				m.Tstart = t
			}
		}
	}

	return m
}

func (s *DataStore) groupFromMin(minute int) int {
	return ((minute % 60) / s.interval) * s.interval
}

func (s *DataStore) filenameFromTime(t time.Time) string {
	t = t.UTC().Truncate(time.Duration(s.interval) * time.Minute)
	return fmt.Sprintf("%s/%02d/%04d-%03d-%02d.bin",
		s.rootDir,
		t.Minute(),
		t.Year(),
		t.YearDay(),
		t.Minute())
}

// AddRecord adds a new Record to the Store. The file is opened, the
// new record appended, the contents are synced to the disk, and the file
// is closed. This is not designed for "high speed" operation.
func (s *DataStore) AddRecord(rec *Record) error {
	ts := time.Unix(rec.Time, 0).UTC()
	if s.meta.Tstart.IsZero() || ts.Before(s.meta.Tstart) {
		s.meta.Tstart = ts
	}

	if ts.After(s.meta.Tend) {
		s.meta.Tend = ts
	}

	path := s.filenameFromTime(ts)
	s.fs.MkdirAll(filepath.Dir(path), 0755)
	f, err := s.fs.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = rec.WriteTo(f)
	if err != nil {
		return err
	}
	s.meta.Nrecs++

	return nil
}

// View provides access to all of the data records for a
// single group
type View struct {
	fs    afero.Fs
	group int
	dir   string
}

// Date returns a new time.Time value from the string YYYY-mm-dd
func Date(date string) (time.Time, error) {
	return time.Parse("2006-01-02", date)
}

// NewView returns a new view for the given minute of the hour
func (s *DataStore) NewView(minute int) View {
	v := View{fs: s.fs, group: s.groupFromMin(minute)}
	v.dir = filepath.Join(s.rootDir, fmt.Sprintf("%02d", v.group))
	return v
}

func (v View) fileName(t time.Time) string {
	return fmt.Sprintf("%04d-%03d-%02d.bin", t.Year(), t.YearDay(), v.group)
}

// Walk returns a channel which will supply succesive Record values from the
// view until the records are exhausted or the Context is cancelled. The
// records will start at the beginning of the date specified by dstart and
// will run for a maximum of ndays days. If ndays <= 0, the records will run
// until the end of the DataStore.
func (v View) Walk(ctx context.Context, dstart time.Time, ndays int) chan *Record {
	ch := make(chan *Record, 1)
	start := v.fileName(dstart)

	go func() {
		defer close(ch)
		count := 0
		afero.Walk(v.fs, v.dir, func(path string, fi os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if fi.Name() < start {
				return nil
			}
			if ndays > 0 && count >= ndays {
				return filepath.SkipDir
			}
			select {
			case <-ctx.Done():
				return ctx.Err()
			default:
			}

			in, err := afero.ReadFile(v.fs, path)
			if err != nil {
				return nil
			}
			store := &Store{}
			if err := proto.Unmarshal(in, store); err != nil {
				return nil
			}

			count++
			for _, rec := range store.Records {
				select {
				case ch <- rec:
				case <-ctx.Done():
					return ctx.Err()
				}
			}

			return nil
		})
	}()

	return ch
}

// WriteTo implements the io.WriterTo interface, it appends the Record
// to an io.Writer as a repeated Protocol Buffer message.
func (r *Record) WriteTo(w io.Writer) (int64, error) {
	out, err := proto.Marshal(r)
	if err != nil {
		return 0, err
	}

	// Append the new record in Protocol Buffer key-value format. The
	// value part is the varint-encoded length of the record followed
	// by the record contents.
	const key = uint64((1 << 3) | 2)
	buf := make([]byte, binary.MaxVarintLen64)

	n := binary.PutUvarint(buf, key)
	w.Write(buf[:n])
	n = binary.PutUvarint(buf, uint64(len(out)))
	w.Write(buf[:n])

	n, err = w.Write(out)
	return int64(n), err
}

// func (req *DataReq) StartDate() time.Time {
// 	return time.Date(int(req.Year), time.Month(req.Month+1), int(req.Day),
// 		0, 0, 0, 0, time.UTC)
// }

// func (req *DataReq) EndDate() time.Time {
// 	t := req.StartDate()
// }

// // InRange returns true if the DataReq falls within the DataRange
// func (req *DataReq) InRange(dr *DataRange) bool {
// }

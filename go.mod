module bitbucket.org/uwaploe/abprdata

go 1.13

require (
	github.com/golang/protobuf v1.4.3
	github.com/spf13/afero v1.5.1
	google.golang.org/protobuf v1.25.0
)
